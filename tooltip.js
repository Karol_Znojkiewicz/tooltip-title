(function () {
    let activeTooltip = null;

    const showTooltip = (e) => {
        const pos = e.currentTarget.getBoundingClientRect();
        const options = {
            w: pos.width,
            x: pos.left,
            y: pos.top,
        };

        const text = e.currentTarget.getAttribute('title');
        createTooltip(text, options);

        e.currentTarget.removeAttribute('title');
    };
    const createTooltip = (textTooltip, options) => {
        let createDiv = document.createElement('div');
        createDiv.textContent = textTooltip;
        createDiv.classList.add('edu-tooltip', 'hidden');
        document.body.append(createDiv);
        createDiv.style.left = `${
            options.x + options.w / 2 - createDiv.offsetWidth / 2
        }px`;
        createDiv.style.top = `${options.y - createDiv.offsetHeight - 10}px`;
        createDiv.classList.remove('hidden');

        activeTooltip = createDiv;
    };
    const hiddenTooltip = (e) => {
        if (activeTooltip) {
            e.currentTarget.setAttribute('title', activeTooltip.textContent);
            activeTooltip.remove();
        }
    };
    const init = () => {
        let elems = document.querySelectorAll('[title]');
        for (let elem of elems) {
            elem.addEventListener('mouseenter', showTooltip);
            elem.addEventListener('mouseleave', hiddenTooltip);
        }
    };

    init();
})();
